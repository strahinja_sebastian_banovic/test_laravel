@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
      <div class="col-xs-10 col-xs-offset-1">
        @if (session('flash_message'))
          <div class="alert alert-success">{{ session('flash_message') }}</div>
        @endif
        @if (session('error_message'))
          <div class="alert alert-danger">{{ session('error_message') }}</div>
        @endif
      </div>
    </div>

    @if(count($errors))
    <div class="row">
      <div class="col-md-6 col-md-offset-2 text-center">
        <div class="alert alert-danger">
          @foreach($errors->all() as $error)
          <p>{{ ($error) }}</p>
          @endforeach
        </div>
      </div>
    </div>
    @endif

    <div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#add-teacher">
				<i class="fa fa-user"></i> Add Teacher
			</button>
			<div class="modal fade" id="add-teacher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Add new Teacher</h4>
						</div>
						<form action="{{url('/store_teacher')}}" method="post">
							<div class="modal-body">
								<input name="_token" type="hidden" value="{{ csrf_token() }}">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-offset-2 col-md-8">
												<input type="text" name="first_name" class="form-control" placeholder="First Name" required>
											</div>
										</div>
										<div class="row">
											<div class="col-md-offset-2 col-md-8">
												<input type="text" name="last_name" class="form-control" placeholder="Last Name" required>
											</div>
										</div>
										<div class="row">
											<div class="col-md-offset-2 col-md-8">
												<input type="date" name="birth_date" class="form-control" placeholder="Birth Date" required>
											</div>
										</div>
										<div class="row">
											<div class="col-md-offset-2 col-md-8">
												<select name="school_name" class="form-control" required>
													@foreach($schools as $school)
														<option value="{{$school->id}}"> {{$school->school_name}} </option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-default fa fa-plus" data-submit="modal">Add</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
    </div>

    <hr>

    <div class="row">
    	<form action="{{url('search')}}" method="post">
			<input name="_token" type="hidden" value="{{ csrf_token() }}">
			<div class="col-xs-3 col-xs-offset-1">
      	   		<input placeholder="Search" type="text" name="search" class="form-control" value="">
			</div>

			<div class="col-xs-3 col-xs-offset-1">
				<input type="submit" class="btn btn-primary" value="Search">
			</div>
    	</form>
	</div>
	
	<hr>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Teacher List</div>
				<div class="panel-body">
					<table class="table table-striped">
						<thead>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Birth Date</th>
							<th>School Name</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</thead>
						<tbody>
							@foreach ($teachers as $teacher)
							<tr>
								<td>
									<div>{{ $teacher->first_name }}</div>
								</td>
								<td>
									<div>{{ $teacher->last_name }}</div>
								</td>
								<td>
									<div>{{ $teacher->birth_date }}</div>
								</td>
								<td>
									<div>{{ $teacher->school_name }}</div>
								</td>
								<td>
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#teacher-{{ $teacher->id }}">
									  <i class="fa fa-eye"></i>
									</button>
									<div class="modal fade" id="teacher-{{ $teacher->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">{{ $teacher->first_name }} {{ $teacher->last_name }}</h4>
												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-md-12">
															<table class="table table-striped table-condensed">
																<tr>
																	<th>Name</th>
																	<td>{{ $teacher->first_name }} {{ $teacher->last_name }}</td>
																</tr>
															    <tr>
																	<th>Birth Date</th>
																	<td><p>{{ $teacher->birth_date }}</p></td>
																</tr>
																<tr>
																	<th>School</th>
																	<td>{{ $teacher->school_name }}</td>
																</tr>
															</table>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</td>
								<td>
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#teacherEdit-{{ $teacher->id }}">
									  <i class="fa fa-edit"></i>
									</button>
									<div class="modal fade" id="teacherEdit-{{ $teacher->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">{{ $teacher->first_name }} {{ $teacher->last_name }}</h4>
												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-md-12">
															<form action="{{url('update_teacher')}}" method="post">
																<input name="_token" type="hidden" value="{{ csrf_token() }}">
																<input name="id" type="hidden" value="{{ $teacher->id }}">
																<div class="row">
																	<div class="col-md-offset-2 col-md-8">
																		<input type="text" name="first_name" class="form-control" value="{{ $teacher->first_name }}" required>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-offset-2 col-md-8">
																		<input type="text" name="last_name" class="form-control" value="{{ $teacher->last_name }}" required>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-offset-2 col-md-8">
																		<input type="date" name="birth_date" class="form-control" value="{{ $teacher->birth_date }}" required>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-offset-2 col-md-8">
																		<select name="school_name" class="form-control" required>
																			@foreach($schools as $school)
																				<option value="{{$school->id}}" @if($school->id == $teacher->school_id) selected @endif> {{$school->school_name}} </option>
																			@endforeach
																		</select>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-offset-2 col-md-8">
																		<input type="submit" class="btn btn-default" value="Save">
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</td>
								<td>
									<form action="{{ url('delete_teacher') }}" method="post">
										<input name="_token" type="hidden" value="{{ csrf_token() }}">
										<input name="id" type="hidden" value="{{$teacher->id}}">
										<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
