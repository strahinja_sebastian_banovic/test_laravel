@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
      <div class="col-xs-10 col-xs-offset-1">
        @if (session('flash_message'))
          <div class="alert alert-success">{{ session('flash_message') }}</div>
        @endif
        @if (session('error_message'))
          <div class="alert alert-danger">{{ session('error_message') }}</div>
        @endif
      </div>
    </div>

    @if(count($errors))
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center">
        <div class="alert alert-danger">
          @foreach($errors->all() as $error)
          <p>{{ ($error) }}</p>
          @endforeach
        </div>
      </div>
    </div>
    @endif

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Create School</div>
				<div class="panel-body">
					<form action="{{url('store_school')}}" method="post">
						<input name="_token" type="hidden" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col-md-offset-2 col-md-8">
								<input type="text" name="school_name" class="form-control" placeholder="School Name" required>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-2 col-md-8">
								<input type="text" name="year_founded" class="form-control" placeholder="Year Founded" required>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-2 col-md-8">
								<input type="text" name="city" class="form-control" placeholder="City">
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-2 col-md-8">
								<input type="submit" class="btn btn-default" value="Submit">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
@endsection
