<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Teacher;
use App\School;
use DB;

class TeacherController extends Controller
{
    public function store(Request $request) {
		$request->validate([
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
			'birth_date' => 'required|date',
			'school_name' => 'required|integer',
		]);
		
		$teacher = new Teacher;
		
		$teacher->first_name = $request->first_name;
		$teacher->last_name = $request->last_name;
		$teacher->birth_date = $request->birth_date;
		$teacher->school_id = $request->school_name;
		
		$teacher->save();
		
		Session::flash('flash_message', 'Teacher succesfully added!');
		
		return redirect('teachers');
	}
	
	public function update(Request $request) {
		$request->validate([
			'id' => 'required',
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
			'birth_date' => 'required|date',
			'school_name' => 'required|integer',
		]);
		
		DB::table('teachers')
			->where('id', $request->id)
            ->update([
				'first_name' => $request->first_name,
				'last_name' => $request->last_name,
				'birth_date' => $request->birth_date,
				'school_id' => $request->school_name
				]);
		
		Session::flash('flash_message', 'Teacher succesfully updated!');
		
		return redirect('teachers');
	}
	
	public function delete(Request $request) {
		$request->validate([
			'id' => 'required',
		]);
		
		DB::table('teachers')->where('id', $request->id)->delete();
		
		Session::flash('flash_message', 'Teacher succesfully deleted!');
		
		return redirect('teachers');
	}
	
	public function search(Request $request) {
		
		$teachers = DB::table('teachers')
			->leftjoin('schools', 'teachers.school_id', '=', 'schools.id')
			->where('first_name', 'like',  '%'.$request->search.'%')
			->orWhere('last_name', 'like',  '%'.$request->search.'%')
			->select('teachers.*', 'schools.school_name')
			->get();
		$schools = School::all();
		
		return view('teachers', compact('teachers', 'schools'));
	}
}
