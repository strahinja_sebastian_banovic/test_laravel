<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Teacher;
use App\School;
use DB;

class HomeController extends Controller
{	
	public function teachers() {
		$teachers = DB::table('teachers')->join('schools', 'teachers.school_id', '=', 'schools.id')->select('teachers.*', 'schools.school_name')->get();
		$schools = School::all();
		return view('teachers', compact('teachers', 'schools'));
	}
}
