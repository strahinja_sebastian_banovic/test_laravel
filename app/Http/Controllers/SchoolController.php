<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\School;

class SchoolController extends Controller
{
    public function create() {
		return view('create_school');
	}
	
	public function store(Request $request) {
		$request->validate([
			'school_name' => 'required|max:255',
			'year_founded' => 'required|integer',
		]);
		
		$school = new School;
		
		$school->school_name = $request->school_name;
		$school->year_founded = $request->year_founded;
		$school->city = $request->city;
		
		$school->save();
		
		Session::flash('flash_message', 'School succesfully added!');
		
		return redirect('create_school');
	}
}
